#BlastTV

* BlastTV is a pipeline by using BLAST to do Taxonomy profiling result Validation.
* Algorithm:
* Version: 0.1

##Installing TargetedNGS
Download the latest version of BlastTV from [bitbucket](https://bitbucket.org/chienchilo/blasttv/downloads/) or use `git clone` from command line.

```
git clone https://chienchilo@bitbucket.org/chienchilo/blasttv.git
```
##Dependencies
BlastTV run requires following dependencies which should be in your path.

### Programming/Scripting languages
- [Perl >=v5.18.2](https://www.perl.org/get.html)
    - The pipeline has only been tested in v5.18.2

### Unix
- sort

### Third party softwares/packages
- [Blast+ (2.5.0)](https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download)

### Database configuration
- [Taxonomy: taxdb](ftp://ftp.ncbi.nih.gov/blast/db/)
- [NT](ftp://ftp.ncbi.nlm.nih.gov/blast/db/)

(Optional)

- [WGS](ftp://ftp.ncbi.nlm.nih.gov/blast/WGS_TOOLS/README_BLASTWGS.txt)

## Running BlastTV

```
Usage:
    BlastAutoPercent --in FASTQ_FILE --target "TARGET ORGANISM"

```

## Test

```
cd test
../BlastAutoPercent --in 510_024_Haemophilus_parainfluenzae.fastq --target "Haemophilus parainfluenzae"

```

## Outputs
```
Determination on whether the target organism is in the sample: Yes, No, or Maybe
Number of reads: number
Number of reads that called the target organism: number
Number of reads that called a different organism or several organisms that were not the target: number
Number of reads that called several organisms including the target (ambiguous reads): number

Different organisms called:
Organism Name

#Reads organized by reads that didn't call target, ambiguous reads, then reads that called the target
Read Name
Organism Name: Score

```

## Contact Info

- Challacombe, Zoe Merced: <zchallacombe@lanl.gov>
- Chien-Chi Lo: <chienchi@lanl.gov>